package ru.shokin.tm.service;

import ru.shokin.tm.repository.HistoryRepository;

public class HistoryService {

    private final HistoryRepository historyRepository;

    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    public void create(final String command) {
        if (command == null || command.isEmpty()) return;
        historyRepository.create(command);
    }

    public void clear() {
        historyRepository.clear();
    }

    public void viewHistory() {
        historyRepository.viewHistory();
    }

}