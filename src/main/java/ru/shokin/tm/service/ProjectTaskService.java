package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.entity.Task;
import ru.shokin.tm.repository.ProjectRepository;
import ru.shokin.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public void addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    public void removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    public void removeProjectWithTasks(final Long userId, final Long projectId) {
        final List<Task> tasks = findAllByProjectId(projectId);
        if (tasks == null) return;
        for (final Task task : tasks) {
            final long taskId = task.getId();
            taskRepository.removeById(userId, taskId);
        }
    }

}