package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final Long userId, final String name, final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(userId, name, description);
    }

    public Project update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project removeById(final Long userId, final Long id) {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.removeById(userId, id);
    }

    public Project removeByName(final Long userId, final String name) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(userId, name);
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByUserIdAndId(final Long userId, final Long id) {
        if (userId == null || id == null) return null;
        return projectRepository.findByUserIdAndId(userId, id);
    }

    public Project findByUserIdAndName(final Long userId, final String name) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByUserIdAndName(userId, name);
    }

    public List<Project> findByUserIdAndNameFromMap(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByUserIdAndNameFromMap(name);
    }

    public List<Project> findAllByUserId(final Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

}