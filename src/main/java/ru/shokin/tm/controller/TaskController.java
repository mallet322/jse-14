package ru.shokin.tm.controller;

import ru.shokin.tm.entity.Task;
import ru.shokin.tm.service.ProjectTaskService;
import ru.shokin.tm.service.TaskService;
import ru.shokin.tm.service.UserService;

import java.util.Comparator;
import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService,
                          UserService userService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() {
        System.out.println("Create task");
        System.out.println("Please, enter task name:");
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description:");
        final String description = scanner.nextLine();
        if (taskService.create(userId, name, description) == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return -1;
        }
        System.out.println("ok");
        return 0;
    }

    public int clearTask() {
        System.out.println("Clear task");
        taskService.clear();
        System.out.println("ok");
        return 0;
    }

    public int listTask() {
        System.out.println("Tasks list");
        final Long userId = userService.currentUser.getId();
        viewTasks(taskService.findAllByUserId(userId));
        System.out.println("ok");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("View task");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("***");
        System.out.println("User ID: " + task.getUserId());
        System.out.println("ok");

    }

    public int viewTaskById() {
        System.out.println("Please, enter task id:");
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Task task = taskService.findByUserIdAndId(userId, id);
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() {
        System.out.println("Please, enter task name:");
        final String name = scanner.nextLine();
        viewTasksByName(taskService.findByUserIdAndNameFromMap(name));
        return 0;
    }

    public int updateTaskById() {
        System.out.println("Update task by id");
        System.out.println("Please, enter task id:");
        final Long userId = userService.currentUser.getId();
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findByUserIdAndId(userId, id);
        if (task == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter new task name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new task description:");
        final String description = scanner.nextLine();
        taskService.update(id, name, description);
        System.out.println("ok");
        return 0;
    }

    public int updateTaskByName() {
        System.out.println("Update task by name");
        System.out.println("Please, enter task name:");
        final Long userId = userService.currentUser.getId();
        final String taskName = scanner.nextLine();
        final Task task = taskService.findByUserIdAndName(userId, taskName);
        if (task == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter new task name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new task description:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("ok");
        return 0;
    }

    public int removeTaskById() {
        System.out.println("Remove task by id");
        System.out.println("Please, enter task id:");
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(userId, id);
        if (task == null) {
            System.out.println("failed");
        } else {
            System.out.println("ok");
        }
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("Remove task by name");
        System.out.println("Please, enter task name:");
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(userId, name);
        if (task == null) {
            System.out.println("failed");
        } else {
            System.out.println("ok");
        }
        return 0;
    }

    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        tasks.sort(Comparator.comparing(Task::getName));
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ": " + task.getId() + " " + task.getName());
            index++;
        }
    }

    public void viewTasksByName(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        for (final Task task : tasks) {
            System.out.println("ID: " + task.getId());
            System.out.println("NAME: " + task.getName());
            System.out.println("DESCRIPTION: " + task.getDescription());
            System.out.println("User ID: " + task.getUserId());
            System.out.println();
        }
    }

    public int listTaskByProjectId() {
        System.out.println("List task by project");
        System.out.println("Please, enter project id:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, but this project has no tasks...");
            return 0;
        } else {
            viewTasks(tasks);
            System.out.println("ok");
        }
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("Add task to project by ids");
        System.out.println("Please, enter project id:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter task id:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("ok");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("Remove task from project by ids");
        System.out.println("Please, enter project id:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter task id:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("ok");
        return 0;
    }

}