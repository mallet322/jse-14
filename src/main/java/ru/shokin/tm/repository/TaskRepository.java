package ru.shokin.tm.repository;

import ru.shokin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    private final Map<String, List<Task>> taskMap = new HashMap<>();

    private void addToMap(Task task) {
        String taskName = task.getName();
        List<Task> taskList = new ArrayList<>();
        if (taskMap.get(taskName) == null) {
            taskList.add(task);
            taskMap.put(taskName, taskList);
        } else {
            taskList = taskMap.get(taskName);
            taskList.add(task);
        }
    }

    public Task create(final Long userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long id, String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public void clear() {
        tasks.clear();
        taskMap.clear();
    }

    public Task removeById(final Long userId, final Long id) {
        final Task task = findByUserIdAndId(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        String name = task.getName();
        taskMap.remove(name);
        return task;
    }

    public Task removeByName(final Long userId, final String name) {
        final Task task = findByUserIdAndName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        taskMap.remove(name);
        return task;
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task findByUserIdAndId(final Long userId, final Long id) {
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByUserIdAndName(final Long userId, final String name) {
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public List<Task> findByUserIdAndNameFromMap(final String name) {
        return taskMap.get(name);
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

}