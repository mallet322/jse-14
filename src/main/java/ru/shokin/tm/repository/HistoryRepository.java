package ru.shokin.tm.repository;

import java.util.LinkedList;

public class HistoryRepository {

    private final LinkedList<String> history = new LinkedList<>();

    public void create(final String command) {
        history.add(command);
        final int limit = 10;
        if (history.size() > limit) {
            history.removeFirst();
        }
    }

    public void clear() {
        history.clear();
    }

    public void viewHistory() {
        if (history.isEmpty()) return;
        int index = 1;
        for (final String command : history) {
            System.out.println(index + ". " + command);
            index++;
        }
    }

}